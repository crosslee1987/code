package com.lee.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

//@Aspectj
@Component
public class AspectjConfig {

    //匹配包下面所有类所有方法
//    @Before("execution(* com.spring.aspect.*.*(..))")
    //匹配包下面某个类所有方法
//@Before("execution(* com.spring.aspect.PutDemo.*(..))")
    //匹配包下面某个类以某个字母开头方法
//    @Before("execution(* com.spring.aspect.PutDemo.ea*(..))")


    LocalDateTime localDateTimeBefore = null;
    LocalDateTime localDateTimeAfter = null;

    @Autowired
    private RedisTemplate redisTemplate;

    private static int count = 0;

//    @Before("execution(* com.lee.controller.HelloController.show(..))")
//    public void printBefore() {
//        localDateTimeBefore = LocalDateTime.now();
//        System.out.println(localDateTimeBefore.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//    }
//
//
//    @After("execution(* com.lee.controller.HelloController.show(..))")
//    public void printAfter() {
//        localDateTimeAfter = LocalDateTime.now();
//        System.out.println(localDateTimeAfter.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//        Duration duration = Duration.between(localDateTimeBefore,localDateTimeAfter);
//        System.out.println(duration.toMillis());
//    }

//    @Around("execution(* com.lee.controller.HelloController.show(..))")
//     public void printAroud(ProceedingJoinPoint  joinpoint) throws Throwable {
//        joinpoint.proceed();
//        System.out.println("printAroud");
//    }

    //    @AfterThrowing("execution(* com.lee.controller.*.*(..)))")
//    public void show() {
//         System.out.println("hellothrow");
//    }

    //    @After("execution(* com.lee.controller.HelloController.show(..))")
    public void countHello() {
        System.out.println(count);
        redisTemplate.opsForValue().set("count", ++count);

    }

}
