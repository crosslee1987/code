package com.lee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/hello")
    public String show() {
        System.out.println("+++hello");
        return "hello";
    }

    public static void main(String[] args) {
        HelloController helloController = new HelloController();
        helloController.show();
    }

    @RequestMapping("/count")
    public String count() {
        Object count = redisTemplate.opsForValue().get("count");
        return Integer.toString((Integer) count);
    }

    @RequestMapping("/clear")
    public void clear() {
        redisTemplate.opsForValue().set("count", 0);
    }
}
