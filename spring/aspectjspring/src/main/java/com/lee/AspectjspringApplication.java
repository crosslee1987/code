package com.lee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AspectjspringApplication {

    public static void main(String[] args) {
        SpringApplication.run(AspectjspringApplication.class, args);
    }

}
