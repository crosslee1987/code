package com.lee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.servlet.Servlet;

@SpringBootApplication
public class JavaApplication  {

    public static void main(String[] args) {
        SpringApplication.run(JavaApplication.class, args);
    }

}
