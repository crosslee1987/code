package com.lee.proxy;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyText implements ProxyDemo {

    public void show() {
        System.out.println("吃饭");
    }

    public static void main(String[] args) {
        ProxyText proxyText = new ProxyText();
//        ProxyTextTwo proxyTextTwo = new ProxyTextTwo();
//        ProxyDemo proxyDemo = (ProxyDemo) proxyTextTwo.getObject(proxyText);
//        proxyDemo.show();

        ProxyText object = (ProxyText) new ProxyTextThree().getObject(proxyText);
        object.show();
    }

}

interface ProxyDemo {
    public void show();
}


class ProxyTextTwo implements InvocationHandler {

    Object object = new Object();

    public Object getObject(Object o) {
        this.object = o;
        return Proxy.newProxyInstance(o.getClass().getClassLoader(), o.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        begin();
        Object invoke = method.invoke(object, args);
        end();
        return invoke;
    }

    public void begin() {
        System.out.println("前置调用");
    }

    public void end() {
        System.out.println("后置调用");
    }
}

class ProxyTextThree implements MethodInterceptor {

    private Object object;

    public Object getObject(Object o) {
        this.object = o;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(o.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        begin();
        Object invoke = method.invoke(object, objects);
        end();
        return invoke;
    }

    public void begin() {
        System.out.println("前置调用");
    }

    public void end() {
        System.out.println("后置调用");
    }
}