package com.lee.thread;

import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.util.concurrent.Callable;

public class ThreadText {

    public static void main(String[] args) throws Exception {
//        ThreadOne threadOne = new ThreadOne();
//        threadOne.start();
//
//        Thread thread = new Thread(new ThreadRunnable());
//        thread.start();
//
//        ThreadCall threadCall = new ThreadCall();
//        System.out.println(threadCall.call());
//
//        Thread thread1 = new Thread(() -> {
//            Object lock = new Object();
//            try {
//                synchronized (lock) {
//                    lock.wait(5000);
//                    lock.notify();
//                    System.out.println("执行前");
//                    Thread.sleep(1000);
//                    System.out.println("执行后");
//                }
//
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//        });
//        thread1.start();
////        thread1.join();
//        System.out.println(thread1.getName());
//        System.out.println("主线程");

        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(33);
        });

        Thread thread2 = new Thread(() -> {
            System.out.println(22);
        });
        thread.start();
        thread.join();
        thread2.start();
        System.out.println(44);
    }

}


class ThreadOne extends Thread {

    public void run() {
        Object lock = new Object();
        synchronized (lock) {
            try {
                lock.wait(10000);
                lock.notify();
                System.out.println("thread");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}

class ThreadRunnable implements Runnable {

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("runnable");
    }
}

class ThreadCall implements Callable {

    @Override
    public Object call() throws Exception {
        return "call";
    }
}