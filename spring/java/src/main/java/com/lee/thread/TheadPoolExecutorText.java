package com.lee.thread;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.concurrent.TimeUnit.SECONDS;

public class TheadPoolExecutorText {


    public static void main(String[] args) {
        ThreadPoolExecutor threadPoolExecutor =
                new ThreadPoolExecutor(1, 1, 10, SECONDS, new LinkedBlockingDeque<>(2));
        threadPoolExecutor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("threadpoolexecutor");
            }
        });
        threadPoolExecutor.shutdownNow();
    }
}


class ThreadRunable implements Runnable {

    @Override
    public void run() {
        System.out.println("threadrunable");
    }
}

