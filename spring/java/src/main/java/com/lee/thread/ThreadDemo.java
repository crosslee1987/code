package com.lee.thread;

import lombok.extern.log4j.Log4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

public class ThreadDemo {

    private static int count = 0;
    private static ReentrantLock reentrantLoc = new ReentrantLock();
    private volatile int state = 0;

    public static void main(String[] args) throws Exception {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        AtomicInteger atomicInteger = new AtomicInteger();
        for (int i = 0; i < 1; i++) {
            System.out.println(2 >> 2);
            Thread thread = new Thread(() -> {
                try {
                    countDownLatch.await();
                    for (int j = 0; j < 1000; j++) {
                        reentrantLoc.lock();
                        add();
                        reentrantLoc.unlock();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            });
            thread.start();
        }
        Thread.sleep(500);
        countDownLatch.countDown();
        Thread.sleep(2000);
        System.out.println(count);
    }

    public synchronized static void add() {
        count++;
    }
}
