package com.lee.sort;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SortDemo {

    static Integer[] a = {1, 3, 4, 10, 11, 5, 11, 22, 0, 2};

    public static void main(String[] args) {
        sort2(a);
    }

    public static void sort1(int[] arr) {
        int flag = 0;
        int key = 0;
        int kk = 0;
        for (int i = 0; i < arr.length; i++) {
            flag = arr[i];
            for (int j = i; j < arr.length; j++) {
                if (flag > arr[j]) {
                    flag = arr[j];
                    key = j;
                }
            }
            if (flag < arr[i]) {
                kk = arr[i];
                arr[i] = arr[key];
                arr[key] = kk;
            }

        }
//        for (int i = 0; i < arr.length; i++) {
//            System.out.println(arr[i]);
//        }
    }

    public static void sort2(Integer[] arr) {
        int flag;
        int kk;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    flag = a[i];
                    a[i] = a[j];
                    a[j] = flag;
                }
            }
        }
//        System.out.println(2<<1);
//        for (int i = 0; i < arr.length; i++) {
//            System.out.println(arr[i]);
//        }


        List<User> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setName("lee" + i);
            user.setPhone("123" + i);
            list.add(user);
        }

        List<String> lst = list.stream().map(m -> m.getName()).collect(Collectors.toList());

        User user = list.stream().filter(m -> "lee1".equals(m.getName())).findFirst().orElse(new User());

    }
}

@Data
class User {
    private String name;
    private String phone;
}
