package com.lee.sort;

public class FindBinSearch {

    public static void main(String[] args) {
        int srcArray[] = {3, 5, 11, 17, 21, 23, 28, 30, 32, 50, 64, 78, 81, 95, 101};
        System.out.println(binSearch(srcArray, 5));
    }

    public static int binSearch(int[] arr, int key) {
        int start = 0;
        int end = arr.length;
        boolean flag = true;
        int mid = (end - start) / 2;

        while (flag) {
            if (key < arr[mid]) {
                end = mid;
                mid = (mid - start) / 2;
            } else if (key > arr[mid]) {
                mid = (end - mid) / 2 + mid;
            } else if (key == arr[mid]) {
                flag = false;
                return mid;
            }
        }
        return 0;
    }
}

