package com.lee.map;

import com.lee.ann.UserSerivce;

import java.util.*;

public class J_Map {

    public static void main(String[] args) {
        Map map = new HashMap();
        map.put("lee", "001");
        map.put("lee1", "002");

        List list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);

        Set set = new TreeSet<User>();
        set.add(new User("lee1"));
        set.add(new User("lee2"));

        set.add(new User("lee3"));


        list.forEach(item -> {
            System.out.println(item);

        });

        set.forEach(item -> {
            User user = (User) item;
            user.show();
        });

    }
}

class User {
    private String name;

    public User(String name) {
        this.name = name;
    }

    public void show() {
        System.out.println(name);
    }
}
