package com.lee.ann;

import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ServiceLee("lee")
class Father {
    public void say(String name) {
        System.out.println("我是father叫" + name);
    }
}

public class Text {

    public void test() throws Exception {
        Class clz = Class.forName("com.lee.ann.UserSerivce");
        Object obj = clz.newInstance();
        Method method = clz.getMethod("showName", String.class);
        method.invoke(obj, "nihao");

        Method method1 = clz.getMethod("showPhone", String.class, String.class);
        method1.invoke(obj, "lee", "chao");

        ServiceLee annotation = method.getAnnotation(ServiceLee.class);
        System.out.println(annotation.value());

        Service service = (Service) clz.getAnnotation(Service.class);
        System.out.println(service.value());

//        List<UserSerivce> list = new ArrayList<>();
//        list.add(new UserSerivce("lee1","123"));
//        list.add(new UserSerivce("lee2","1234"));
//        list.forEach(i -> System.out.println(i.getName()));
//
//        List list1 = list.stream().map(iter ->iter.getName()).collect(Collectors.toList());
//
//        UserSerivce name =  list.stream().filter(iter ->iter.getName().equals("lee1")).findFirst().orElse(null);

//        System.out.println(name.getName());

    }

    public static void main(String[] args) throws Exception {
        new Text().test();
    }

}

