package com.lee.design;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DesignText {

    public static void main(String[] args) {
//        DemoFactroy demoFactroy = new DemoFactroy();
//        System.out.println(demoFactroy.hashCode());
//        demoFactroy.getDemo(new DemoOne()).show();

        Map map = new HashMap<>();
        map.put("lee", "001");
        List lst = new ArrayList<>();
        lst.add("lee");
    }

}


class SinglObject {
    private static SinglObject singlObject = new SinglObject();

    public static SinglObject getSinglObject() {
        return singlObject;
    }
}

interface Demo {
    public void show();
}

class DemoOne implements Demo {
    public void show() {
        System.out.println("demoone");
    }
}

class DemoTwo implements Demo {
    public void show() {
        System.out.println("demoTwo");
    }
}

class DemoFactroy {
    Demo getDemo(Demo demo) {
        if (demo instanceof DemoOne) {
            return new DemoOne();
        } else {
            return new DemoTwo();
        }
    }
}