package com.lee.lock;

import sun.font.TextRecord;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class J_Atomicinteger {

    static int count = 0;
    static int state = 0;
    static AtomicInteger atomicInteger = new AtomicInteger(0);
    static AtomicBoolean atomicBoolean = new AtomicBoolean(true);
    static boolean flag = true;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                flag = atomicBoolean.get();
                if (atomicBoolean.compareAndSet(flag, false)) {
                    for (int j = 0; j < 100; j++) {
                        count++;
                    }
                }
            }).start();
        }
        Thread.sleep(3000);
        System.out.print(count);
    }

}

class User {
    UserOne userOne = new UserOne();

    void show() {
        userOne.show();
    }
}

class UserOne {
    UserTwo userTwo = new UserTwo();

    void show() {
        userTwo.show();
    }
}

class UserTwo {
    void show() {
        System.out.print("usertwo");
    }
}
