package com.lee.lock;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

public class ReenTLock {

    static int count = 0;

    private static volatile int state = 0;

    public static void main(String[] args) throws InterruptedException {
        ReentrantLock reentrantLock = new ReentrantLock();

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    reentrantLock.lock();
                    add();
                    reentrantLock.unlock();
                }
            }).start();

        }
        Thread.sleep(10000);
        System.out.print(count);
    }

    public static void add() {
        count++;
    }
}


