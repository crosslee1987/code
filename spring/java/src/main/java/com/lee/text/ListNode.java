package com.lee.text;

import lombok.Data;

public class ListNode {


    public static void main(String[] args) {
        Node nodeA = new Node();
        nodeA.setI(1);
        Node nodeB = new Node();
        nodeB.setI(2);
        Node nodeC = new Node();
        nodeC.setI(3);
        nodeA.setNext(nodeB);
        nodeB.setNext(nodeC);

        nodeC.setNext(nodeA.getNext());
        nodeB.setNext(nodeA);

    }
}

@Data
class Node {
    int i = 0;
    Node next;
}