package com.lee;

import com.lee.annotation.ComponentScanLee;
import com.lee.boot.SpringApplicationLee;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@ComponentScanLee("com.lee")
public class SpringbootApplication {

    public static void main(String[] args) {
        SpringApplicationLee.run(SpringbootApplication.class, args);
    }

}
