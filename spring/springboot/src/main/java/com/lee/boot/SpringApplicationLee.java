package com.lee.boot;

import com.lee.annotation.ComponentScanLee;

public class SpringApplicationLee {

    public static void run(Class<?> clas,String...args) {
        String value = clas.getAnnotation(ComponentScanLee.class).value();
        System.out.println(value);
    }
}
