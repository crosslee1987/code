package com.lee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/producer")
public class Hello {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/producer")
    public String show() {
        System.out.println("进入方法+++");
        return "hello";
    }

    @RequestMapping("/producer1")
    public String show1() {
        return "hello1";
    }

    @RequestMapping("/consumer")
    public String consumer() {
        //通过网关调用
        return restTemplate.postForEntity("http://localhost:9988/consumer/consumer/consumer", null, String.class).getBody();

        //通过注册中心调用
        //return restTemplate.postForEntity("http://consumer/consumer/consumer/consumer", null, String.class).getBody();

        //通过nginx远程调用
        //return restTemplate.postForEntity("http://localhost/consumer/consumer/consumer", null, String.class).getBody();
    }
}
