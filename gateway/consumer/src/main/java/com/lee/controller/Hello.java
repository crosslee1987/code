package com.lee.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumer")
public class Hello {

    @RequestMapping("/consumer")
    public String show() {
        System.out.println("进入方法consumer+++");
        return "hello";
    }

    @RequestMapping("/consumer1")
    public String show1() {
        return "hello1";
    }
}
